package hello.springtx.propagation;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MemberServiceFacade {

    private final MemberService memberService;
    private final LogRepository logRepository;

    public void join(String username){

        try{
            memberService.joinV2(username);
            logRepository.save(new Log(username));
        }catch (RuntimeException e){
            log.info("로그 예외 발생한것을 구조를 바꿔서 잡았다.");
        }

        
    }
}

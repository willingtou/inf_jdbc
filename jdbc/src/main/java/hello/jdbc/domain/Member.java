package hello.jdbc.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Member {

    private String memberId;
    private int money;

}
